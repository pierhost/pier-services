from os import environ, path
import sys
from utils import merge_dicts, install_file_with_content, read_config
from yaml import dump
from passwords import passwordManager
from importlib import import_module
from functools import reduce
from exit_codes import ExitCode
import logging

logger = logging.getLogger("log")

class FacilitiesWrapper:
    def __init__(self, stack):
        self.stack = stack
        self.srv_config = ServiceConfig(stack)
        self.pass_manager = passwordManager(self.srv_config.secrets_dir, self.srv_config.dest_dir + '/.env')
        self.injectors = {}

        # Idea:
        #   - find filenames to import and store them in a variable
        #   - use X = __import__('var')
        #   - use inspect module find classname and then create an instance and assign to injectors[service]
        for service_name in self.srv_config.get_services_to_install():
            sys.path.append(self.srv_config.specs_dir)
            modfile = self.srv_config.specs_dir + "/" + service_name + "/integration/injector.py"
            module =  service_name + ".integration.injector"
            if path.isfile(modfile):
                logger.debug("Detected an injector for service %s", service_name)
                X = import_module(module)
                self.injectors[service_name] = X.Injector()

        self.__docker_compose = {}

    def init_env(self):
        # Copy env file
        file = open(self.srv_config.env_file, "r")
        environment = file.read()
        file.close()
        environment += '\nCOMPOSE_PROJECT_NAME=pier_' + self.stack + '\n'
        install_file_with_content(self.srv_config.dest_dir, '.env', environment, 0o600)

    def inject_config(self, config):
        merge_dicts(self.__docker_compose, config)

    def get_current_compose_value(self, access_path):
        current = self.__docker_compose
        for access_key in access_path:
            if access_key in current:
                current = current[access_key]
            else:
                return None
        return current

    def dump_compose_to(self, dest_file):
        install_file_with_content( \
            self.srv_config.dest_dir, dest_file, \
            dump(self.__docker_compose, default_flow_style=False, width=200) , 0o644)

class ServiceConfig:
    defaults = {
        'SERVICES_SPECS_DIR': './pier-services/Specs',
        'ENV_FILE': './.env',
        'SECRETS_DIR':"./Secrets",
        'SERVICES_DEST_DIR':"./Generated",
        'USER_CONFIG_FILE':"./config.yml",
        # TODO: if we use directly the pier-services config and we change it (e.g. https in traefik), see what will happen when we update pier-services
        'CRITICAL_CONFIG_FILE':"./pier-services/critical_config.yml",
        'ADMIN_CONFIG_FILE':"./pier-services/admin_config.yml"
    }
    def __init__(self, kind):
        self.kind = kind
        self.define_base_paths()
        self.config = {"services": {}}
        merge_dicts(self.config, read_config(self.config_file))

    def get_config_for(self, service_name):
        return self.config["services"][service_name]

    def will_service_be_installed(self, service_name):
        if not (service_name in self.config["services"]):
            return False
        service_specific_config = self.config["services"][service_name]
        if not ("activated" in service_specific_config) or not service_specific_config["activated"]:
            return False
        else:
            return True

    def filter_with_warnings(self, service_name):
        is_installed = self.will_service_be_installed(service_name)
        if not is_installed:
            logger.info("[%s]: Service is not activated, skipping", service_name)
        return is_installed

    def define_base_paths(self):
        # Check specs directory
        if 'SERVICES_SPECS_DIR' in environ:
            self.specs_dir = environ.get('SERVICES_SPECS_DIR')
        else:
            self.specs_dir = self.defaults['SERVICES_SPECS_DIR']
            logger.info('No spec file source directory specified. Assuming %s. To change, pass SERVICES_SPECS_DIR as an environment variable', self.defaults['SERVICES_SPECS_DIR'])
        if not (path.isdir(self.specs_dir)):
            logger.critical('The directory specified to source the spec files does not exist. Please pass a valid SERVICES_SPECS_DIR as an environment variable')
            exit(ExitCode.BAD_ENVIRONMENT_VAR)

        # Check docker environment file
        if 'ENV_FILE' in environ:
            self.env_file = environ.get('ENV_FILE')
        else:
            self.env_file = self.defaults['ENV_FILE']
            logger.info('No docker env file specified. Assuming %s. To change, pass ENV_FILE as an environment variable', self.defaults['ENV_FILE'])
        if  not (path.isfile(self.env_file)):
            logger.critical('The env file specified is not valid. Please pass a valid ENV_FILE as an environment variable')
            exit(ExitCode.BAD_ENVIRONMENT_VAR)

        # Check secrets directory
        if 'SECRETS_DIR' in environ:
            self.secrets_dir = environ.get('SECRETS_DIR')
        else:
            self.secrets_dir = self.defaults['SECRETS_DIR']
            logger.info('No secrets directory specified. Assuming %s. To change, pass SECRETS_DIR as an environment variable', self.defaults['SECRETS_DIR'])
        if 'SERVICES_DEST_DIR' in environ:
            self.dest_dir = environ.get('SERVICES_DEST_DIR')
        else:
            self.dest_dir = self.defaults['SERVICES_DEST_DIR']
            logger.info('No destination directory specified. Assuming %s. To change, pass SERVICES_DEST_DIR as an environment variable', self.defaults['SERVICES_DEST_DIR'])
        self.dest_dir += '/' + self.kind

        cfg_file = self.kind.upper() + '_CONFIG_FILE'
        if cfg_file in environ:
            self.config_file = environ.get(cfg_file)
            if  not (path.isfile(self.config_file)):
                logger.warning("The specified user config file, %s, does not exist. Make sure that a proper file is passed as %s", self.config_file, cfg_file)
        else:
            self.config_file = self.defaults[cfg_file]
            if  not (path.isfile(self.config_file)):
                logger.warning("The default user config file, %s, does not exist. Make sure that a proper file is passed as %s", self.config_file, cfg_file)


    def get_services_to_install(self):
        services_config_obj = self.config["services"]
        if services_config_obj:
            return filter( self.filter_with_warnings, services_config_obj.keys())
        return []

    def check_fullfillment(self, condition):
        all_fullfilled = True
        for service_name in condition:
            service_specific_config = self.config["services"][service_name]
            service_requirements = condition[service_name]
            # recursively check if user config matches condition
            def rec(config, cond):
                recursion_result = []
                for k in cond:
                    if k not in config:
                        recursion_result.append(False)
                    elif type(cond[k]) is dict:
                        recursion_result.append(rec(config[k], cond[k]))
                    else:
                        recursion_result.append(config[k] == cond[k])
                return reduce(lambda a, b: (a and b), recursion_result)
            all_fullfilled = rec(service_specific_config, service_requirements)
        return all_fullfilled
